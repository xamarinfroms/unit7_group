﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinUniversity.Interfaces;

namespace Unit5_DataBinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Products : ContentPage
    {
        public Products(INavigationService nav)
        {
            InitializeComponent();
            var viewModel = new ProductsViewModel() { NavigationService = nav };
            this.BindingContext = viewModel;



            BindingBase.EnableCollectionSynchronization(
                viewModel.Products, 
                null,
                (datas, context, action, writeAccess) =>
                {
                    lock (datas)
                    {
                        action();
                    }
                });

            ListView list = new ListView(ListViewCachingStrategy.RetainElement);
  
        }

        private async void list_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var vm = this.BindingContext as ProductsViewModel;
            if (vm.OnGoDetail != null && vm.OnGoDetail.CanExecute(e.Item))
            {
              
                

                vm.OnGoDetail.Execute(e.Item);
            }
        }

        private void list_Refreshing(object sender, EventArgs e)
        {
            //更新資料...
            //...
          //  this.list.IsRefreshing = false;
        }
    }

    [ContentProperty("Source")]
    public class ImageMak : IMarkupExtension
    {
        public string Source { get; set; }
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return ImageSource.FromResource($"Unit5_DataBinding.Images.{Source}");
        }
    }
    public class ProductTemplateSelector : DataTemplateSelector
    {
        public DataTemplate Normal { get; set; }
        public DataTemplate Soldout { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var data = item as ProductViewModel;
            return data.Count == 0 ? Soldout : Normal;
        }
    }
}
