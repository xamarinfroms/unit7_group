﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit5.Core
{
    public  class LoginData
    {
        

        public string Account { get; set; }

        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
