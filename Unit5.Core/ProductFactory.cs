﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit5.Core
{
    public class ProductFactory
    {
        public static IList<Product> Products
        {
            get 
            {
                if (_products == null)
                    _products = CreateProducts();
                return _products;

                

            }
        }

        private static IList<Product> _products;
        public static IList<Product> CreateProducts()
        {


            return new List<Product>()
            {
                new Product() {  ProductId=Guid.NewGuid(), ImageName="01.jpg", Name="貓咪飲水機", Desc="貓用飲水機，喝水喝到飽", Price=2000   },
                 new Product() {  ProductId=Guid.NewGuid(), ImageName="02.jpg", Name="貓用腳踏墊", Desc="放在貓砂盆前，防止貓砂到處掉落", Price=200   },
                  new Product() {  ProductId=Guid.NewGuid(), ImageName="03.jpg", Name="貓用搶劫頭套", Desc="做壞事的時候可以用，但好像看不到路", Price=500,Count=0   },
                   new Product() {  ProductId=Guid.NewGuid(), ImageName="04.jpg", Name="貓咪抱枕", Desc="中指貓咪的圖案，送禮自用兩相宜", Price=199   },
                    new Product() {  ProductId=Guid.NewGuid(), ImageName="05.jpg", Name="貓抓柱", Desc="雖然有貓抓柱，但貓還是都去抓沙發呢?", Price=299, IsOnSale=true   },
                     new Product() {  ProductId=Guid.NewGuid(), ImageName="06.jpg", Name="寵物用飲食碗", Desc="你看看這麼高級的碗，比人用的還貴", Price=499,IsOnSale=true   },
                      new Product() {  ProductId=Guid.NewGuid(), ImageName="07.jpg", Name="貓罐頭", Desc="朕餓了，拿吃的來", Price=35   },
                       new Product() {  ProductId=Guid.NewGuid(), ImageName="08.jpg", Name="貓屋", Desc="對於貓來說，有個貓屋是很重要的，這樣他才有地方藏東西，然後還是在棉被裡睡...", Price=3500   },
                        new Product() {  ProductId=Guid.NewGuid(), ImageName="09.jpg", Name="貓跳台", Desc="貓跳台解決了許多貓咪運動不足的問題，只要買了這個跳台，包準你的貓一定會瘦下來!無效不退費", Price=800   },
                         new Product() {  ProductId=Guid.NewGuid(), ImageName="10.jpg", Name="多拉A夢", Desc="多拉A夢是許多人的童年，你看看這麼有創意的造型，還不買嗎", Price=2000   },
                          new Product() {  ProductId=Guid.NewGuid(), ImageName="11.jpg", Name="自動雷射", Desc="該產品可讓貓咪追著抓不到的雷射光跑，本產品保證雷射可以射出範圍達100公里，並且可以射穿鐵、鋼等物質。", Price=4000   },
                           new Product() {  ProductId=Guid.NewGuid(), ImageName="12.png", Name="貓咪頭套", Desc="你以為這樣我就看不出你是貓了嗎", Price=2000   },
                            new Product() {  ProductId=Guid.NewGuid(), ImageName="13.jpg", Name="貓咪玩具", Desc="本產品使用反地心引力原理，並且搭配慣性原理，讓玩具移動繞圈在產品上，無需任何電力，只要開始轉動永遠不會停，最大速度可達光速", Price=699   } ,
                           new Product() {  ProductId=Guid.NewGuid(), ImageName="14.jpg", Name="驚嚇的貓咪", Desc="....我想這應該是非賣品", Price=99999   },
                            new Product() {  ProductId=Guid.NewGuid(), ImageName="15.jpg", Name="貓咪雨傘", Desc="有了中指貓咪抱枕，當然還要來一個中指貓咪雨傘，本雨傘具有神奇的功能，當傘打開時雨就會停止，但若收傘會繼續下雨，請保持雨傘開啟", Price=1500   },
                             new Product() {  ProductId=Guid.NewGuid(), ImageName="16.jpg", Name="噴火槍", Desc="貓咪造型的噴火槍，讓許多人都愛不釋手，可以用來生日點蠟燭，點菸，或者當作暖爐使用都是不錯的方式", Price=1000,Count=0   }
            };
        }
    }
}
